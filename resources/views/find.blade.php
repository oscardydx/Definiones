<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
   
    <body>
        

            
                <div class="title m-b-md">
                    Diccionario
                </div>

            	<table class="table table-striped">
		<thead>
			<th scope="col">Palabra</th>
			<th scope="col">Significado</th>
	
		</thead>
		<tbody>
			@foreach($Palabras as $word)
                @php
                    $result=explode("=",$word);
                    $palabre=$result[0];
                    if(isset($result[1])){$def=$result[1];}else{$def="";};
                @endphp
              
				<tr>
					<th scope="row">{{ $palabre }}</th>
					<td>{{ $def }}</td>
		
					

				</tr>
			@endforeach
		</tbody>
	
	</table>
                

        
        
    </body>
</html>
