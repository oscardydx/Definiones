<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
class APIController extends Controller
{
  
    public function index(Request $request)
    {   $Result="";
        $Palabras=explode(",", $request->input);
        $cont2=0;
        function quitarestiqutasCuadradas($x){
            $quitarEtiqueta=explode("[[", $x);
            if(count($quitarEtiqueta)>1){
    
                $quitarEtiqueta2=explode("]]", $x);
                $palabramedia=explode("[[",  $quitarEtiqueta2[0]);
                $restante="";
                $cont=1;
                foreach($quitarEtiqueta2 as $stringEti){
                    if($cont>1 && $cont<count($quitarEtiqueta2)){
                    $restante.=$stringEti."]]";}
                    else if($cont==count($quitarEtiqueta2))
                    {
                        $restante.=$stringEti;}
                        $cont++;
                }
    
                $quitarEtiqueta3=explode("|", $palabramedia[1]);  
                if(count($quitarEtiqueta3)>1){
                    //dd($quitarEtiqueta3);
                    $x=$quitarEtiqueta[0].$quitarEtiqueta3[1].$restante;
                }else{
                //  dd($palabramedia[1]);
                    $x=$quitarEtiqueta[0].$palabramedia[1].$restante;
                }
            
            }
    
            return $x;
      
        }
        function quitarestiqutasCorchetes($x){
            $quitarEtiqueta=explode("{{", $x);
            if(count($quitarEtiqueta)>1){
    
                $quitarEtiqueta2=explode("}}", $x);
                $palabramedia=explode("{{",  $quitarEtiqueta2[0]);
                $restante="";
                $cont=1;
                foreach($quitarEtiqueta2 as $stringEti){
                    if($cont>1 && $cont<count($quitarEtiqueta2)){
                    $restante.=$stringEti."}}";}
                    else if($cont==count($quitarEtiqueta2))
                    {
                        $restante.=$stringEti;}
                        $cont++;
                }
    
                $quitarEtiqueta3=explode("|", $palabramedia[1]);  
                if(count($quitarEtiqueta3)>1){
                    //dd($quitarEtiqueta3);
                    $x=$quitarEtiqueta[0].$quitarEtiqueta3[1].$restante;
                }else{
                //  dd($palabramedia[1]);
                    $x=$quitarEtiqueta[0].$palabramedia[1].$restante;
                }
            
            }
    
            return $x;
        }

        foreach($Palabras as $word){
           
            $query='https://es.wiktionary.org/w/api.php?format=phpfm&action=query&titles=';
            $query.=strtolower($word);
            $query.='&rvprop=content&prop=revisions&redirects=1';
            $product = Http::post($query);
       
            $Definition = explode(";1", $product->body());

            if(count($Definition)>1){
                try{
                    $Definition = explode("{{clear}}", $Definition[1]);
                }
                catch (Throwable $t)
                {
                    
                    $Definition = explode("=== Información adicional ===", $Definition[1]);
                }
       
                $Definition = explode("\n", $Definition[0]);
             
                //quitar etiquetas html href
                            
                $quitar=explode("&lt;ref", $Definition[0]);
                if(count($quitar)>1){
                    $quitar2=explode("/ref>", $Definition[0]);
                    if(count($quitar2)>1){
                    $Definition[0]=$quitar[0].$quitar2[1];}
                }

           

                    $pruebaEtiqueta=explode("[[", $Definition[0]);

                    if(count($pruebaEtiqueta)>1){
                        for ($i = 1; $i < count($pruebaEtiqueta); $i++) {
                            $Definition[0]= quitarestiqutasCuadradas($Definition[0]);
                        }
                
                    }
                    $pruebaEtiqueta=explode("{{", $Definition[0]);

                    if(count($pruebaEtiqueta)>1){
                        for ($i = 1; $i < count($pruebaEtiqueta); $i++) {
                            $Definition[0]= quitarestiqutasCorchetes($Definition[0]);
                        }
                
                    }
                $word .= '= ';
    
           
                

                $Result=substr($Definition[0],1);
           
                $word .= $Result;
                $Palabras[$cont2]=$word;
                //dd($cont2);
              }
              $cont2=$cont2+1;
        }
        //dd($Palabras);
        return view('find')->with(['Palabras'=>$Palabras]);
    }

}
